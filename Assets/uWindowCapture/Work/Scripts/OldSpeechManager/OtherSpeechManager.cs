﻿using UnityEngine;
using NMeCab;

/// <summary>
/// このクラスは、形態素解析を利用した入力処理。
/// 複雑になるので、作ってみたが、保留しておく。
/// 問題が多いからだ。たとえば南極に替えたくない、とか南極に行きたいとは思わない。のような場合でも南極に変えてしまう。
/// 南極という単語さえ入っていれば変わってしまう。全てのパターンに対応するよう作られていない。
/// 
/// 音声入力にintelRealSenceを使っている。
/// なぜならwindowSpeechはオフラインでは使えないという情報があったからだ。
/// しかし試してみたらオフラインでも使えたので、メインの方ではまだwindowSpeechを利用している。
/// 残してあるのは後学のため。
/// </summary>

public class OtherSpeechManager : MonoBehaviour
{
    private PXCMSession session;
    private PXCMAudioSource source;
    private PXCMSpeechRecognition sr;
    private PXCMSpeechRecognition.Handler handler;

    private MeCabParam meCabParam;
    private MeCabTagger meCabTagger;

    private string inputText = "";
    public string getInputText
    {
        get
        {
            return inputText;
        }
    }

    private int speechCount = 0;
    public int getSpeechCount
    {
        get
        {
            return speechCount;
        }
    }

    public void Start()
    {
        meCabCreateInstance();
        speechCreateInstance();
    }

    private void meCabCreateInstance()
    {
        meCabParam = new MeCabParam();
        meCabParam.DicDir = @"Assets/NMeCab0.07/dic/ipadic";
        meCabTagger = MeCabTagger.Create(meCabParam);
    }

    private void speechCreateInstance()
    {
        session = PXCMSession.CreateInstance();
        source = session.CreateAudioSource();

        PXCMAudioSource.DeviceInfo dinfo = null;

        source.QueryDeviceInfo(1, out dinfo);
        source.SetDevice(dinfo);
        Debug.Log(dinfo.name);

        session.CreateImpl<PXCMSpeechRecognition>(out sr);

        PXCMSpeechRecognition.ProfileInfo pinfo;
        sr.QueryProfile(out pinfo);
        pinfo.language = PXCMSpeechRecognition.LanguageType.LANGUAGE_JP_JAPANESE;
        sr.SetProfile(pinfo);

        handler = new PXCMSpeechRecognition.Handler();
        //handler.onRecognition = (x) => Debug.Log(x.scores[0].sentence);
        handler.onRecognition = inputSpeechJudgment;
        sr.SetDictation();
        sr.StartRec(source, handler);
    }

    private void inputSpeechJudgment(PXCMSpeechRecognition.RecognitionData data)
    {
        string sentence = data.scores[0].sentence;
        Debug.Log(sentence);

        MeCabNode node = meCabTagger.ParseToNode(sentence);

        inputText = null;
        while (node != null)
        {
            if (node.CharType > 0)
            {
                Debug.Log(node.Surface);
                inputText = speechJudgment(node.Surface);
                if (inputText != null)
                {
                    speechCount++;
                    break;
                }
            }
            node = node.Next;
        }
        Debug.Log("");
    }

    void OnDisable()
    {
        if (sr != null)
        {
            sr.StopRec();
            sr.Dispose();
        }

        if (session != null)
            session.Dispose();
    }

    private bool desktop;
    private bool window;
    private bool open;
    private bool close;

    private string speechJudgment(string text)
    {
        switch (text)
        {
            case "デスクトップ":
                desktop = true;
                break;
            case "ウィンドウ":
                window = true;
                break;
            case "開い":
                open = true;
                break;
            case "閉じ":
                close = true;
                break;
            case "ネクスト":
                return SpeechText.NEXT;
            case "南極":
                return SpeechText.ANTARCTIC;
            case "山小屋":
                return SpeechText.HUT;
            case "ショパン":
                return SpeechText.CHOPIN;
            case "雨音":
                return SpeechText.RAIN;
            case "ミュート":
                return SpeechText.MUTE;
        }

        if (desktop)
        {
            return openOrClose(SpeechText.D_OPEN, SpeechText.D_CLOSE);
        }
        else if (window)
        {
            return openOrClose(SpeechText.W_OPEN, SpeechText.W_CLOSE);
        }
        else
        {
            return "";
        }
    }

    private string openOrClose(string opening, string closing)
    {
        if (open)
        {
            return opening;
        }
        else if (close)
        {
            return closing;
        }
        else
        {
            return "";
        }
    }

}