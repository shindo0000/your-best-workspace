﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeManager : MonoBehaviour
{
    private float start;
    private float end;
    private float time;
    [SerializeField] private Image image;

    public void fadeStart(float startAlpha, float endAlpha, float fadeTime)
    {
        start = startAlpha;
        end = endAlpha;
        time = fadeTime;
        StartCoroutine("fade");
    }

    private IEnumerator fade()
    {
        float elapsedTime = 0.0f;
        while (elapsedTime < time)
        {
            elapsedTime += Time.deltaTime;
            var newColor = image.color;
            newColor.a = Mathf.Lerp(start, end, Mathf.Clamp01(elapsedTime / time));
            image.color = newColor;
            yield return new WaitForEndOfFrame();
        }
    }
}
