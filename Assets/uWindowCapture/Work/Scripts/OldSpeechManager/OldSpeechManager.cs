﻿using UnityEngine;
using UnityEngine.Windows.Speech;

/// <summary>
/// windows speech はアプリにフォーカスがあたっていない場合にマイク入力がされなくなるので不採用となった。
/// </summary>
namespace uWindowCapture
{

    // 音声認識マネージャー
    public class OldSpeechManager : MonoBehaviour
    {
        private KeywordRecognizer recognizer;

        private int speechCount = 0;
        public int getSpeechCount
        {
            get
            {
                return speechCount;
            }
        }

        private string inputText = "";
        public string getInputText
        {
            get
            {
                return inputText;
            }
        }

        void Start()
        {
            // 音声コマンド登録
            var words = new SpeechText().getSpeechTextList;
            recognizer = new KeywordRecognizer(words);
            recognizer.OnPhraseRecognized += OnPhraseRecognized;
            recognizer.Start();
        }

        private void OnPhraseRecognized(PhraseRecognizedEventArgs args)
        {
            print(args.text);
            speechCount++;
            inputText = args.text;
        }
    }
}
