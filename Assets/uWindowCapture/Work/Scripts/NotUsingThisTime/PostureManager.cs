﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PostureManager : MonoBehaviour
{
    [SerializeField] Transform cameraTf;
    [SerializeField] GameObject postureCanvas;

    private Vector3 correctPosition;
    private const float ACCEPTABLE_RANGE = 0.02f;

    // Update is called once per frame
    void Update()
    {
        var cameraPosition = cameraTf.position;

        if (OVRInput.GetDown(OVRInput.RawButton.A))
        {
            correctPosition = cameraPosition;
        }

        if ((cameraPosition.z - correctPosition.z) > 0 && (correctPosition.y - cameraPosition.y) > ACCEPTABLE_RANGE)
        {
            postureCanvas.SetActive(true);
        }
        else
        {
            postureCanvas.SetActive(false);
        }
    }
}
