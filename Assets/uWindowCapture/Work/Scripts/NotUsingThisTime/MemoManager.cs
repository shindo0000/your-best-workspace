﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MemoManager : MonoBehaviour
{
    [SerializeField] List<Transform> memoTf;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("punch");
    }

    private IEnumerator punch()
    {
        yield return new WaitForSeconds(4.0f);

        this.transform.DOPunchRotation(punch: new Vector3(20f, 0f, 20f), duration: 1.0f);
        yield return new WaitForSeconds(1.0f);
        Sequence seq = DOTween.Sequence();

        Vector3[] paths = new Vector3[] { memoTf[0].localPosition, memoTf[1].localPosition };
        seq.Append(this.transform.DOLocalPath(paths, 2.0f, PathType.CatmullRom));
        seq.Join(this.transform.DORotate(endValue: memoTf[1].localEulerAngles, duration: 2.0f, mode: RotateMode.FastBeyond360));
        seq.Play();

    }

}
