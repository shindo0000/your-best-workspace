﻿using UnityEngine;

namespace uWindowCapture {

    // メインプログラムとは関係ないチュートリアルのためだけのスクリプト
    public class TutorialManager : MonoBehaviour
    {

        [SerializeField] GameObject startUI;
        [SerializeField] Transform comment;
        private GameObject[] texts;

        private MainManager mainManager;
        private string inputText = "";
        private string[] tutorialTexts;

        private bool started;

        // チュートリアル経過時間（speechManagerで音声認識がされるなどのイベントがある度にカウントされていく）
        private int counted = 999;
        private int count = 0;
        public int Count
        {
            set
            {
                count = value;
            }
            get
            {
                return count;
            }
        }

        // Start is called before the first frame update
        void Start()
        {
            started = false;

            texts = new GameObject[comment.childCount];
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i] = comment.GetChild(i).gameObject;
            }

            mainManager = GetComponent<MainManager>();

            foreach (var textObj in texts)
            {
                textObj.SetActive(false);
            }
            texts[0].SetActive(true);

            tutorialTexts = new []{
                SpeechText.NEXT,
                SpeechText.D_OPEN,
                SpeechText.W_OPEN,
                SpeechText.NEXT,
                SpeechText.ANTARCTIC,
                SpeechText.HUT,
                SpeechText.CHOPIN,
                SpeechText.RAIN, };
        }

        // Update is called once per frame
        void Update()
        {
            if (!started)
            {
                if (OVRInput.Get(OVRInput.RawButton.A))
                {
                    started = true;
                    startUI.SetActive(false);
                }
            }
            else
            {

                inputText = mainManager.getInputText == "" ? "" : mainManager.getInputText;

                if (count != counted)
                {
                    tutorial();
                }
            }

        }

        private void tutorial()
        {
            for (int i = 0; i < tutorialTexts.Length; i++)
            {
                if (count == i && inputText == tutorialTexts[i])
                {
                    changeText();
                    break;
                }
            }
        }

        private void changeText()
        {
            counted = count;

            texts[count].SetActive(false);
            texts[++count].SetActive(true);
        }
    }
}
