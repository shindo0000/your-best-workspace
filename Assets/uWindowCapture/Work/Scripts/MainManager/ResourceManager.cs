﻿using UnityEngine;
using UnityEngine.Rendering;


// 音楽およびステージの管理スクリプト
public class ResourceManager : MonoBehaviour
{
    [SerializeField] Transform stage;
    [Header("Stage"),Space(3)]
    [SerializeField] Transform initialStage;
    [SerializeField] Transform nextStage;
    [SerializeField] GameObject directionLight;
    [Header("BGM"), Space(3)]
    [SerializeField] AudioSource audioSource;
    [SerializeField] AudioClip initialBGM;
    [SerializeField] AudioClip nextBGM;


    void Awake()
    {
        setFalseToActiveStage();
        initialStage.gameObject.SetActive(true);
    }

    private void Start()
    {
        directionLight.SetActive(false);
        RenderSettings.ambientIntensity = 0.3f;

        audioSource.clip = initialBGM;
        audioSource.Play();
    }

    private void setFalseToActiveStage()
    {
        for (int i = 0; i < stage.childCount; i++)
        {
            if (stage.GetChild(i).gameObject.activeSelf)
            {
                stage.GetChild(i).gameObject.SetActive(false);
            }
        }
    }

    public void changeStageView(string changeToStage)
    {
        setFalseToActiveStage();
        switch (changeToStage)
        {
            case "field":
                directionLight.SetActive(true);
                nextStage.gameObject.SetActive(true);
                RenderSettings.ambientIntensity = 1.0f;

                break;
            case "Level":
                directionLight.SetActive(false);
                initialStage.gameObject.SetActive(true);
                break;
        }
    }

    public void changeBGM(string BGM)
    {
        if (BGM == SpeechText.MUTE)
        {
            audioSource.Stop();
        }
        else
        {
            switch (BGM)
            {
                case SpeechText.CHANGE_BGM:
                    audioSource.clip = nextBGM;
                    break;
            }
            audioSource.Play();
        }
    }
}
