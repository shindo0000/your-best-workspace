﻿// 音声認識文言をまとめたスクリプト。
public class SpeechText
{
    //音声コマンドのキーワード
    public const string D_OPEN = "デスクトップを開いて";
    public const string D_CLOSE = "デスクトップを閉じて";
    public const string W_OPEN = "ウィンドウを開いて";
    public const string W_CLOSE = "ウィンドウを閉じて";
    public const string ANTARCTIC = "南極に変えて";
    public const string HUT = "山小屋に変えて";
    public const string NEXT = "ネクスト";
    // 曲名リストは保留中。
    public const string BGM_OPEN = "曲名リストを開く";
    public const string BGM_CLOSE = "曲名リストを閉じる";
    public const string CHOPIN = "ショパンを流して";
    public const string RAIN = "雨音を流して";
    public const string MUTE = "ミュートにして";

    public const string OK = "了解";
    public const string END = "完了";
    public const string CHANGE_STAGE = "ステージを変えて";
    public const string CHANGE_BGM = "音楽を変えて";


    // キーワードをまとめたリスト
    private string[] speechTextList = new string[] {
                SpeechText.D_OPEN, SpeechText.W_OPEN, SpeechText.D_CLOSE, SpeechText.W_CLOSE,
                SpeechText.ANTARCTIC, SpeechText.HUT, SpeechText.NEXT,
                SpeechText.BGM_OPEN, SpeechText.BGM_CLOSE,
                SpeechText.CHOPIN, SpeechText.RAIN, SpeechText.MUTE};
    public string[] getSpeechTextList
    {
        get
        {
            return speechTextList;
        }

    }
}
