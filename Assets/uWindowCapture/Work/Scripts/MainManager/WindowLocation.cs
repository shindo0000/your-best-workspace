﻿using UnityEngine;
using System.Collections.Generic;

namespace uWindowCapture
{

    // windowの表示場所を管理するスクリプト。
    [RequireComponent(typeof(UwcWindowTextureManager))]
    public class WindowLocation : MonoBehaviour
    {
        [SerializeField] Transform window1;
        [SerializeField] Transform window2;
        private Transform[] windowTransforms;

        private UwcWindowTextureManager manager_;

        private string[] ignoreWindow = { "", "Microsoft Text Input Application" };　// 表示させないwindowのリスト

        private const int windowMinimization = -30000; // windowが最小化されていると座標が-30000以下になっているため。
        private const int _visibleWindowsNumber = 2; // 表示させたいwindowの数。
        public int visibleWindowsNumber
        {
            get
            {
                return _visibleWindowsNumber;
            }
        }

        private bool[] _isOpendeWindow;
        public bool[] isOpendeWindow
        {
            set
            {
                _isOpendeWindow = value;
            }
        }

        private void Start()
        {
            manager_ = GetComponent<UwcWindowTextureManager>();
            windowTransforms = new Transform[] { window1, window2 };
        }

        private void Update()
        {
            // 要らないwindowの削除
            foreach (var item in manager_.windows)
            {
                if (item.Value.window.title == ignoreWindow[0] || item.Value.window.title == ignoreWindow[1])
                {
                    manager_.RemoveWindowTexture(item.Value.window);
                }
            }


            int count = 0;
            for (int i = 0; i < this.transform.childCount; i++)
            {
                this.transform.GetChild(i).GetComponent<UwcWindowTexture>().scaleControlType = WindowTextureScaleControlType.Manual;
                if (this.transform.GetChild(i).GetComponent<UwcWindowTexture>().window.x > windowMinimization ||
                    this.transform.GetChild(i).GetComponent<UwcWindowTexture>().window.y > windowMinimization)
                {
                    if (count < visibleWindowsNumber && _isOpendeWindow[count])
                    {
                        setTransform(i, count);
                    }
                    else
                    {
                        this.transform.GetChild(i).gameObject.SetActive(false);
                    }

                    count++;
                }
                else
                {
                    this.transform.GetChild(i).gameObject.SetActive(false);

                }

            }
        }

        private void setTransform(int index, int count)
        {
            this.transform.GetChild(index).gameObject.SetActive(true);
            this.transform.GetChild(index).SetSiblingIndex(count);
            this.transform.GetChild(index).localPosition = windowTransforms[count].localPosition;
            this.transform.GetChild(index).localRotation = windowTransforms[count].localRotation;
            this.transform.GetChild(index).localScale = windowTransforms[count].localScale;
        }
    }
}