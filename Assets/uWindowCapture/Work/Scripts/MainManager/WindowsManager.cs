﻿using UnityEngine;
using System.Collections.Generic;

namespace uWindowCapture
{
    [RequireComponent(typeof(UwcWindowTextureManager))]

    // desktopとwindowの管理スクリプト
    public class WindowsManager : MonoBehaviour
    {

        [SerializeField] List<Transform> windowLocations;
        [SerializeField] GameObject desktopObj;
        [SerializeField] Transform windowTf;

        // window表示判定
        private bool[] isOpenedWindow;
        private int windowCallCount;

        private UwcWindowTextureManager manager_;

        private string[] ignoreWindow = { "", "Microsoft Text Input Application" };　// 表示させないwindowのリスト

        private const int windowMinimization = -30000; // windowが最小化されていると座標が-30000以下になっているため。
        private int visibleWindowsNumber; // 表示させられるwindowの数。

        // Start is called before the first frame update
        void Start()
        {
            windowCallCount = 0;
            visibleWindowsNumber = windowLocations.Count;

            manager_ = windowTf.GetComponent<UwcWindowTextureManager>();

            isOpenedWindow = new bool[visibleWindowsNumber];
            for (int i = 0; i < visibleWindowsNumber; i++)
            {
                isOpenedWindow[i] = false;
            }
        }

        // Update is called once per frame
        void Update()
        {
            deleteNotNeedWindow();

            // eventが起こった時だけsetせずupdateにしているのは、アクティブ状態で作られるwindowオブジェクトをすぐに非アクティブにしたいから。
            doSetWindowActivate();
        }

        // 要らないwindowの削除
        private void deleteNotNeedWindow()
        {
            // out of sync が出ている。dictionaryの値をforeachで変更できないという意味らしいが、変更をしていないので原因不明
            foreach (var item in manager_.windows.Values)
            {
                var deleteTitle = item.window.title;
                if (deleteTitle == ignoreWindow[0] || deleteTitle == ignoreWindow[1])
                {
                    manager_.RemoveWindowTexture(item.window);
                }
            }
        }

        private void doSetWindowActivate()
        {
            int count = 0;
            for (int i = 0; i < windowTf.childCount; i++)
            {
                windowTf.GetChild(i).GetComponent<UwcWindowTexture>().scaleControlType = WindowTextureScaleControlType.Manual;
                if (windowTf.GetChild(i).GetComponent<UwcWindowTexture>().window.x > windowMinimization ||
                    windowTf.GetChild(i).GetComponent<UwcWindowTexture>().window.y > windowMinimization)
                {
                    if (count < visibleWindowsNumber && isOpenedWindow[count])
                    {
                        windowTf.GetChild(i).gameObject.SetActive(true);
                        windowTf.GetChild(i).SetSiblingIndex(count);
                        windowTf.GetChild(i).localPosition = windowLocations[count].localPosition;
                        windowTf.GetChild(i).localRotation = windowLocations[count].localRotation;
                        windowTf.GetChild(i).localScale = windowLocations[count].localScale;
                    }
                    else
                    {
                        windowTf.GetChild(i).gameObject.SetActive(false);
                    }
                    count++;
                }
                else
                {
                    windowTf.GetChild(i).gameObject.SetActive(false);
                }
            }
        }

        public void doOpenOrCloseDesktop(bool doOpen)
        {
            desktopObj.SetActive(doOpen);
        }

        public void doOpenOrCloseWindow(bool doOpen)
        {
            if (windowCallCount < 0)
            {
                windowCallCount = 0;
            }
            if (windowCallCount >= visibleWindowsNumber)
            {
                windowCallCount = visibleWindowsNumber - 1;
            }

            if (doOpen)
            {
                isOpenedWindow[windowCallCount] = true;
                windowCallCount++;
            }
            else
            {
                isOpenedWindow[windowCallCount] = false;
                windowCallCount--;
            }
        }
    }
}