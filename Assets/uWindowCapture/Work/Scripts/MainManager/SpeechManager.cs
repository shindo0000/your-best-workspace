﻿using UnityEngine;


namespace uWindowCapture
{

    // 音声認識マネージャー
    public class SpeechManager : MonoBehaviour
    {
        private PXCMSession session;
        private PXCMAudioSource source;
        private PXCMSpeechRecognition sr;
        private PXCMSpeechRecognition.Handler handler;
        private PXCMAudioSource.DeviceInfo dinfo;

        private string inputText = "";
        public string propertyInputText
        {
            get
            {
                return inputText;
            }
            set
            {
                inputText = value;
            }
        }

        private int speechCount = 0;
        public int getSpeechCount
        {
            get
            {
                return speechCount;
            }
        }

        public void Start()
        {
            session = PXCMSession.CreateInstance();
            source = session.CreateAudioSource();

            dinfo = null;
            micSet("ヘッドセット マイク (Rift S)");
            if (dinfo == null)
            {
                micSet("マイク (Realtek(R) Audio)");
            }

            //source.QueryDeviceInfo(2, out dinfo);

            source.SetDevice(dinfo);
            Debug.Log(dinfo.name);

            session.CreateImpl<PXCMSpeechRecognition>(out sr);

            PXCMSpeechRecognition.ProfileInfo pinfo;
            sr.QueryProfile(out pinfo);
            pinfo.language = PXCMSpeechRecognition.LanguageType.LANGUAGE_JP_JAPANESE;
            sr.SetProfile(pinfo);

            handler = new PXCMSpeechRecognition.Handler();
            //handler.onRecognition = (x) => Debug.Log(x.scores[0].sentence);
            handler.onRecognition = inputSpeechJudgment;
            sr.SetDictation();
            sr.StartRec(source, handler);
        }

        private void micSet(string micName)
        {
            try
            {
                int i = 0;
                while (true)
                {
                    source.QueryDeviceInfo(i, out dinfo);
                    if (dinfo.name == micName)
                    {
                        break;
                    }
                    i++;
                }

            }
            catch (System.NullReferenceException)
            {
                return;
            }
        }

        private void inputSpeechJudgment(PXCMSpeechRecognition.RecognitionData data)
        {
            string sentence = data.scores[0].sentence;
            Debug.Log(sentence);
            inputText = sentence;
        }

        void OnDisable()
        {
            if (sr != null)
            {
                sr.StopRec();
                sr.Dispose();
            }

            if (session != null)
                session.Dispose();
        }
    }
}

