﻿using UnityEngine;

namespace uWindowCapture {

    // 音声入力から条件分岐をし、各部品の関数を呼び出すメインスクリプト
    public class MainManager : MonoBehaviour
    {
        private WindowsManager windowsManager;
        private SpeechManager speechManager;
        private ResourceManager resourceManager;
        private int speechCalledCount = 999; // 0以外なら問題ない

        private string inputTextForTutorial = null;
        public string getInputText
        {
            get
            {
                return inputTextForTutorial;
            }
        }

        // Start is called before the first frame update
        void Start()
        {
            windowsManager = GetComponent<WindowsManager>();
            speechManager = GetComponent<SpeechManager>();
            resourceManager = GetComponent<ResourceManager>();
        }

        // Update is called once per frame
        void Update()
        {
            if (speechManager.propertyInputText != "") {
                inputTextForTutorial = speechManager.propertyInputText;
                switchText();
            }
        }

        private void switchText()
        {
            switch (speechManager.propertyInputText)
            {
                case SpeechText.D_OPEN:
                    windowsManager.doOpenOrCloseDesktop(true);
                    break;
                case SpeechText.W_OPEN:
                    windowsManager.doOpenOrCloseWindow(true);
                    break;
                case SpeechText.D_CLOSE:
                    windowsManager.doOpenOrCloseDesktop(false);
                    break;
                case SpeechText.W_CLOSE:
                    windowsManager.doOpenOrCloseWindow(false);
                    break;
                case SpeechText.CHANGE_STAGE:
                    resourceManager.changeStageView("field");
                    break;
                case SpeechText.CHANGE_BGM:
                    resourceManager.changeBGM(SpeechText.CHANGE_BGM);
                    break;
                case SpeechText.MUTE:
                    resourceManager.changeBGM(SpeechText.MUTE);
                    break;
            }

            speechManager.propertyInputText = "";

        }
    }
}