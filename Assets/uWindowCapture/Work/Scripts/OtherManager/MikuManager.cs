﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MikuManager : MonoBehaviour
{

    [SerializeField] Animator anim;
    [SerializeField] SkinnedMeshRenderer skinMesh;

    private float waitChangeBoolTime = 1.0f;
    private float waitfinishOjigi = 4.0f;

    public float waitOjigiTime
    {
        get
        {
            return waitChangeBoolTime + waitfinishOjigi;
        }
    }

    public void startOjigiAnime()
    {
        StartCoroutine(ojigi());
        faceChange();
    }

    private IEnumerator ojigi()
    {
        // おじぎ切り替え、あんどidle終了
        anim.SetBool("DoOjigi", true);
        // head look をとめる。
        GetComponent<HeadLookController>().headLookStop = true;
        // この１病またないと、すぐにtrueに戻ってしまい、上に記述したおじぎアニメーションが切り替わらない。
        yield return new WaitForSeconds(waitChangeBoolTime);
        // 1病魔ってから、ふたたびidel つまり trueにする。
        anim.SetBool("DoOjigi", false);
        // おじぎが終わるまでhead look をとめておきたい。だいたい４秒
        yield return new WaitForSeconds(waitfinishOjigi);
        // head look を再開
        GetComponent<HeadLookController>().headLookStop = false;
    }

    private void LateUpdate()
    {
        faceChange();
    }

    // 喋り終わって音声が止まると、まばたきが１００になり、ずっと目をつむる状態になってしまうのを、
    // 無理矢理に開かせている。いずれ、綺麗にしたい。
    // update は lateupdate にしないとなおらない。
    public void faceChange()
    {
        /* blendshapes の名前を表示する。
        var mesh = skinMesh.sharedMesh;
        var shapeCount = mesh.blendShapeCount;
        Debug.LogFormat("Mesh {0} has {1} shapes.", mesh.name, shapeCount);
        for (var i = 0; i < shapeCount; i++)
        {
            Debug.LogFormat("\t{0}: {1}", i, mesh.GetBlendShapeName(i));
        }
        */

        var wink = skinMesh.sharedMesh.GetBlendShapeIndex("0.まばたき");


        if (skinMesh.GetBlendShapeWeight(wink) == 100) {

            int eyeIndex = skinMesh.sharedMesh.GetBlendShapeIndex("0.まばたき");
            skinMesh.SetBlendShapeWeight(eyeIndex, 0);
        }
    }

}
