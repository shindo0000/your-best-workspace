﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TableManager : MonoBehaviour
{
    [SerializeField] Transform leftController;
    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(transform.position.x, leftController.position.y, transform.position.z);
    }
}
