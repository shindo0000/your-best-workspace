﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Pause : MonoBehaviour
{
    [SerializeField] GameObject startUI;
    private bool pause;
    // Start is called before the first frame update
    void Start()
    {
        pause = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (pause)
        {
            if (OVRInput.Get(OVRInput.RawButton.A))
            {
                pause = false;
                startUI.SetActive(false);

            }
        }

        if (pause)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }

        if (OVRInput.Get(OVRInput.RawButton.B))
        {
            Scene scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name);
        }


    }
}
