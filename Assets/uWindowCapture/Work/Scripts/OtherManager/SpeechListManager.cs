﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using uWindowCapture;

public class SpeechListManager : MonoBehaviour
{
    [SerializeField] List<Button> speechList;
    [SerializeField] Text Text;
    [SerializeField] GameObject workSpaceManager;

    private MainManager mainMana;
    private int speechCount = 0;

    private void Start()
    {
        mainMana = workSpaceManager.GetComponent<MainManager>();
    }

    private void Update()
    {
        string inputText = "認識音声：" + mainMana.getInputText;
        Text.text = inputText;
        print(Text.text);
    }

    public void nextSpeechText()
    {
        StartCoroutine(change(speechCount));
        speechCount++;
    }

    private IEnumerator change(int count)
    {
        if (count + 1 > speechList.Count)
        {
            yield break;
        }

        var colors = speechList[count].colors;
        colors.normalColor = new Color(108f / 255f, 206f / 255f, 241f / 255f, 255f / 255f);
        speechList[count].colors = colors;

        var colorsed = speechList[count-1].colors;
        colorsed.normalColor = Color.white;
        speechList[count-1].colors = colorsed;
    }
}
