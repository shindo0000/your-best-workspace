﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HologramManager : MonoBehaviour
{
    private Renderer changeRenderer;
    private float holoWaitTime = 5.0f;
    private float baseWaitTime = 3.0f;

    public float holoFadeTime
    {
        get
        {
            return holoWaitTime + baseWaitTime;
        }
    }

    // Use this for initialization
    void Start()
    {
        changeRenderer = GetComponent<Renderer>();
        changeRenderer.material.EnableKeyword("_EMISSION"); //キーワードの有効化を忘れずに
    }

    public void startFade()
    {
        StartCoroutine("changeAlpha");
    }

    private IEnumerator changeAlpha()
    {
        StartCoroutine(changeFade("_HoloColor", holoWaitTime));
        yield return new WaitForSeconds(holoWaitTime);

        StartCoroutine(changeFade("_Color", baseWaitTime));
    }

    private IEnumerator changeFade(string targetColorName, float time)
    {
        var originalMaterial = new Material(changeRenderer.material);

        float elapsedTime = 0.0f;
        while (elapsedTime < time)
        {
            elapsedTime += Time.deltaTime;
            var newColor = originalMaterial.GetColor(targetColorName);
            newColor.a = Mathf.Lerp(1.0f, 0.0f, Mathf.Clamp01(elapsedTime / time));
            changeRenderer.material.SetColor(targetColorName, newColor);
            yield return new WaitForEndOfFrame();
        }
    }
}