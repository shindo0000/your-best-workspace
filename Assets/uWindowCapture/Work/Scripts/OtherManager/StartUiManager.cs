﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartUiManager : MonoBehaviour
{
    [SerializeField] GameObject textObj;
    [SerializeField] Image fadeImage;

    private bool _started = false;
    public bool started
    {
        get
        {
            return _started;
        }
    }

    private float fadeTime = 5.0f;
    public float getFadeTime
    {
        get
        {
            return fadeTime;
        }
    }

    // Update is called once per frame
    void Update()
    {
        /* フェード処理、今回は無視
         * 
        if (_started)
        {
            return;
        }
        else if (Input.GetKey(KeyCode.Space))
        {
            _started = true;
            textObj.SetActive(false);
            StartCoroutine(fadeStart(1f, 0f, fadeTime));
        }
        */
        _started = true;
    }

    private IEnumerator fadeStart(float start, float end, float time)
    {
        float elapsedTime = 0.0f;
        while (elapsedTime < time)
        {
            elapsedTime += Time.deltaTime;
            var newColor = fadeImage.color;
            newColor.a = Mathf.Lerp(start, end, Mathf.Clamp01(elapsedTime / time));
            fadeImage.color = newColor;
            yield return new WaitForEndOfFrame();
        }
    }

}
