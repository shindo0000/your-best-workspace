﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using uWindowCapture;

public class Tutorial : MonoBehaviour
{
    [SerializeField] GameObject workSpaceManager;
    [SerializeField] GameObject startUI;
    [SerializeField] GameObject miku;
    [SerializeField] GameObject hologram;
    [SerializeField] GameObject speechList;
    [SerializeField] Text startText;
    [SerializeField] List<AudioClip> speechClips;

    private StartUiManager startUiMana;
    private MikuManager mikuMana;
    private HologramManager holoMana;
    private SpeechListManager speechListMana;
    private AudioSource speechAudioSource;
    private MainManager mainMana;

    private int sequence = 0;
    private int speechCount = 0;
    private bool goNextCheck = true;

    private float waitOpenSpeechList = 5.0f;

    private string inputText;
    private string firstTime = "呼ばれるの１回目";
    private string secondTime = "呼ばれるの２回目";

    // Start is called before the first frame update
    void Start()
    {
        startUiMana = startUI.GetComponent<StartUiManager>();
        mikuMana = miku.GetComponent<MikuManager>();
        holoMana = hologram.GetComponent<HologramManager>();
        speechListMana = speechList.GetComponent<SpeechListManager>();
        speechList.SetActive(false);

        speechAudioSource = mikuMana.GetComponent<AudioSource>();
        mainMana = workSpaceManager.GetComponent<MainManager>();
    }

    // Update is called once per frame
    void Update()
    {
        /* フェード処理。今回は無視。
         * 
        if (!startUI.GetComponent<StartUiManager>().started)
        {
            return;
        }
        */

        if(goNextCheck)
        tutorialFlow();

    }

    private void tutorialFlow()
    {
        print(sequence);
        switch (sequence)
        {
            case 0:
                goNextCheck = false;
                StartCoroutine(startHologramFade());
                break;
            case 1:
                goNextCheck = false;
                StartCoroutine(startMikuAction());
                break;
            case 2:
                goNext(SpeechText.OK);
                break;
            case 3:
                processing(2, SpeechText.D_OPEN);
                break;
            case 4:
                processing(3, SpeechText.W_OPEN);
                break;
            case 5:
                processing(4, SpeechText.END);
                break;
            case 6:
                processing(5, SpeechText.CHANGE_STAGE);
                break;
            case 7:
                processing(6, SpeechText.CHANGE_BGM);
                break;
            case 8:
                goNextCheck = false;
                doSpeech(speechCount);
                StartCoroutine(lastProcess());
                break;
            default:
                break;
        }
    }

    private void goNext(string speech)
    {
        if (mainMana.getInputText == speech)
        {
            doNextSpeechList();
            sequence++;
        }
        else if(speech == firstTime)
        {
            sequence++;
            goNextCheck = true;
        }
        else if (speech == secondTime)
        {
            doNextSpeechList();
            sequence++;
            goNextCheck = true;

        }
    }


    private IEnumerator startHologramFade()
    {
        yield return new WaitForSeconds(startUiMana.getFadeTime);
        holoMana.startFade();
        yield return new WaitForSeconds(holoMana.holoFadeTime);
        doSpeech(speechCount);
        goNext(firstTime);
    }

    private IEnumerator startMikuAction()
    {
        mikuMana.startOjigiAnime();
        yield return new WaitForSeconds(mikuMana.waitOjigiTime);
        doSpeech(speechCount);
        yield return new WaitForSeconds(waitOpenSpeechList);
        speechList.SetActive(true);
        goNext(secondTime);

    }

    private void doNextSpeechList()
    {
        speechListMana.nextSpeechText();
    }

    private void doSpeech(int count)
    {
        speechAudioSource.clip = speechClips[count];
        speechAudioSource.Play();
        speechCount++;
    }

    private void processing(int nowSpeechCount, string keyword)
    {
        if (speechCount == nowSpeechCount)
        {
            doSpeech(speechCount);
        }
        goNext(keyword);
    }

    private IEnumerator lastProcess()
    {
        yield return new WaitForSeconds(5f);
        startUI.SetActive(true);
        startText.text = "Bボタンを押してシーンをリロードしてからHMDを外して下さい";
    }
}
